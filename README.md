README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Bulgarian, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present Bulgarian data result from an enhancement of the Bulgarian part of the [PARSEME corpus v 1.1](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to version 1.1, see the change log below.

Corpora
-------
All annotated data come from the [Bulgarian National Corpus](http://dcl.bas.bg/bulnc/en/). Only text samples from the public domain are selected that are not subject to copyright.

For some text samples originality could not be confirmed and there might be some translational texts.


Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated (UDPipe 2).
* UPOS (column 4): Available. Automatically generated (UDPipe 2). The tagset is the one of [UD POS-tags](http://universaldependencies.org/u/pos).
* XPOS (column 5): Available. Automatically generated (UDPipe 2). The tagset is probably the one of Ancora.
* FEATS (column 6): Available. Automatically generated (UDPipe 2). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically generated (UDPipe 2). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* MISC (column 10): Available. Automatically generated (UDPipe 2).
* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, IRV, IAV (experimental annotation for this last category).

IAV have been annotated by some annotators and are thus not fully covered.

The UDPipe annotation relied on the model `bulgarian-btb-ud-2.10-220711`.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

License
-------
The processed and annotated corpus is distributed under the license Creative Commons Attribution 4.0 International [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).


Authors
-------
Ivelina Stoyanova, Svetla Koeva, Svetlozara Leseva, Maria Todorova, Tsvetana Dimitrova, Valentina Stefanova


Contacts
--------
iva@dcl.bas.bg
[http://dcl.bas.bg/](http://dcl.bas.bg/)

[Annotation notes](http://dcl.bas.bg/en/parseme-corpus/)

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - The morphosyntactic annotation (columns UPOS to MISC) from version 1.1 was re-generated with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2/) (bulgarian-btb-ud-2.10-220711 model) by Agata Savary.
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.  
